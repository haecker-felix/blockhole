use std::path::PathBuf;
use std::process::Child;
use std::process::Command;

//pub struct DnsmasqService {
//    child: Option<Child>,
//}

//impl DnsmasqService {
pub fn start(blocklist: PathBuf) {
    //if self.is_running() {
    //    println!("Cannot start dnsmasq service, is already running.")
    //}

    let mut child = Command::new("dnsmasq")
        .args(&[
            &format!("--addn-hosts={}", blocklist.to_str().unwrap()),
            "--domain-needed",
            "--localise-querie",
            "--bogus-priv",
            "--no-resolv",
            "--server=9.9.9.9",
            "--cache-size=10000",
            "--local-ttl=2",
            "--no-daemon",
        ])
        .spawn()
        .expect("Unable to start dnsmasq service");

    //self.child = Some(child);
    println!("Started dnsmasq service");
}

pub fn stop() {
    //if self.is_running() {
    //    if let Some(child) = &mut self.child {
    //        child.kill().expect("Unable to stop dnsmasq service");
    //    }
    //    self.child = None;
    //} else {
    //    println!("Cannot stop dnsmasq service, no service running");
    //}
    //println!("Stopped dnsmasq service");

    let mut child = Command::new("killall")
        .args(&["dnsmasq"])
        .spawn()
        .expect("Unable to stop dnsmasq service");
}

pub fn is_running() -> bool {
    //self.child.is_some()
    false
}
//}
