#[macro_use]
extern crate log;

use clap::{App, Arg};
use std::path::PathBuf;

mod config;
mod dnsmasq;

fn main() {
    let matches = App::new("Blockhole")
        .version(config::VERSION)
        .author(
            "Felix Häcker <haeckerfelix@gnome.org>, Bilal Elmoussaoui <bilelmoussaoui@gnome.org>",
        )
        .about("A system wide adblocker")
        .subcommand(
            clap::SubCommand::with_name("start")
                .about("Start adblock service")
                .arg(
                    Arg::with_name("blocklist")
                        .help("The list with blocked domains")
                        .index(1)
                        .required(true),
                ),
        )
        .subcommand(clap::SubCommand::with_name("stop").about("Stop adblock service"))
        .subcommand(clap::SubCommand::with_name("status").about("Shows current status"))
        .get_matches();

    match matches.subcommand_name() {
        Some("start") => {
            let start_matches = matches.subcommand_matches("start").unwrap();
            let path = start_matches.value_of("blocklist").unwrap();
            dnsmasq::start(PathBuf::from(path));
        }
        Some("status") => (),
        Some("stop") => dnsmasq::stop(),
        _ => {}
    }
}
